from DrissionPage import ChromiumPage
#导入csv模块
import  csv
import  json
#创建文件对象
f = open('data.csv',mode='w',encoding='utf-8',newline='')
csv_writer = csv.DictWriter(f,fieldnames=[
    '用户名',
    '评价分数',
    '评论',
    '日期'
])
csv_writer.writeheader()
#打开浏览器
dp = ChromiumPage()
# 监听数据包
dp.listen.start("/napi/ugcCmtList")
# 访问网站
dp.get("https://hotel.qunar.com/cn/yangzhou/dt-8188/?fromDate=2024-05-05&toDate=2024-05-06")
page = 1
while True:
    # 等待数据包加载
    resp = dp.listen.wait()
    # 获取响应数据内容

    hotelList = resp.response.body["data"]["list"]
    for index in hotelList:

        content = json.loads(index['content'])
        dit = {
            '用户名' : index['nickName'],
            '评价分数' : content['evaluation'],
            '评论': content['feedContent'],
            '日期': content['modtime']
        }
        csv_writer.writerow(dit)

    next_page = dp.ele('css:.next img')
    # if next_page.text == '搜索更多酒店':
    #     # 点击搜索更多酒店
    print(f"第{page}页")
    page+=1
    next_page.click()