import requests
import re

headers = {
    'Cookie' : 'kpf=PC_WEB; clientid=3; did=web_14697b92d05f2687f63e1f2f029e71ae; kpn=KUAISHOU_VISION',
    'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36'
}
#url地址
url = 'https://www.kuaishou.com/graphql'
#构建分页循环
pcursor = ""
for page in range(6):
    print(f'正在采集第{page+1}页内容')
    if page == 0:
        pcursor = ""
    else:
        pcursor = str(page)

    #请求参数
    data = {
        "operationName": "visionSearchPhoto",
        "query":"fragment photoContent on PhotoEntity {\n  __typename\n  id\n  duration\n  caption\n  originCaption\n  likeCount\n  viewCount\n  commentCount\n  realLikeCount\n  coverUrl\n  photoUrl\n  photoH265Url\n  manifest\n  manifestH265\n  videoResource\n  coverUrls {\n    url\n    __typename\n  }\n  timestamp\n  expTag\n  animatedCoverUrl\n  distance\n  videoRatio\n  liked\n  stereoType\n  profileUserTopPhoto\n  musicBlocked\n  riskTagContent\n  riskTagUrl\n}\n\nfragment recoPhotoFragment on recoPhotoEntity {\n  __typename\n  id\n  duration\n  caption\n  originCaption\n  likeCount\n  viewCount\n  commentCount\n  realLikeCount\n  coverUrl\n  photoUrl\n  photoH265Url\n  manifest\n  manifestH265\n  videoResource\n  coverUrls {\n    url\n    __typename\n  }\n  timestamp\n  expTag\n  animatedCoverUrl\n  distance\n  videoRatio\n  liked\n  stereoType\n  profileUserTopPhoto\n  musicBlocked\n  riskTagContent\n  riskTagUrl\n}\n\nfragment feedContent on Feed {\n  type\n  author {\n    id\n    name\n    headerUrl\n    following\n    headerUrls {\n      url\n      __typename\n    }\n    __typename\n  }\n  photo {\n    ...photoContent\n    ...recoPhotoFragment\n    __typename\n  }\n  canAddComment\n  llsid\n  status\n  currentPcursor\n  tags {\n    type\n    name\n    __typename\n  }\n  __typename\n}\n\nquery visionSearchPhoto($keyword: String, $pcursor: String, $searchSessionId: String, $page: String, $webPageArea: String) {\n  visionSearchPhoto(keyword: $keyword, pcursor: $pcursor, searchSessionId: $searchSessionId, page: $page, webPageArea: $webPageArea) {\n    result\n    llsid\n    webPageArea\n    feeds {\n      ...feedContent\n      __typename\n    }\n    searchSessionId\n    pcursor\n    aladdinBanner {\n      imgUrl\n      link\n      __typename\n    }\n    __typename\n  }\n}\n",
        "variables":{"keyword": "腿", "pcursor": pcursor, "page": "search"}
    }
    #response.text 获取响应文本数据 常用于获取网页源代码
    #response.json 获取响应json数据 响应数据必须是完整的json数据格式 数据内容是由{}/[]包裹起来
    #response.content 获取响应二进制数据

    response = requests.post(url=url,headers=headers,json=data)
    json_data = response.json()
    feeds = json_data['data']['visionSearchPhoto']['feeds']
    for feed in feeds:
        #提取视频标题
        title = feed['photo']['caption']
        #替换特殊字符 使用正则
        new_title = re.sub(r'[\\/:*?"<>|\n\r]','',title)
        #提取视频链接
        video_url = feed['photo']['photoUrl']
        #获取视频数据
        video_content = requests.get(url=video_url,headers=headers).content
        #保存数据
        with open('video\\'+new_title+'.mp4',mode='wb') as f:
            #写入数据
            f.write(video_content)
        print(title,video_url)
