import subprocess
import time

import requests
import re
import json


def GetResponse(url):
    """发送请求函数
    - def 关键字用于定义函数
    - GetResponse 函数名自定义变量
    - url形参
    """
    headers = {
        "Cookie": "buvid3=99033FAD-38D8-D76D-CBB6-0F5341F7DAB419244infoc; LIVE_BUVID=AUTO2916327581032608; CURRENT_FNVAL=4048; rpdid=|(um~J|lJu|J0J'uY~lRkk|k|; buvid4=67B36B99-7CF6-39EB-454D-82400C747ED587860-022020722-dIOlI2k6gFl4d2G%2FpF3cNA%3D%3D; header_theme_version=CLOSE; b-user-id=98073fb4-4ab1-efe5-ee1f-abe156259f51; DedeUserID=535196335; DedeUserID__ckMd5=2f50205edcc833ee; enable_web_push=DISABLE; b_nut=100; _uuid=87635A5D-75101-108210-9A10E-6F10105F17428540656infoc; FEED_LIVE_VERSION=V_HEADER_LIVE_NO_POP; PVID=1; bp_video_offset_535196335=924904817625661445; home_feed_column=5; fingerprint=e7cf9c64751eaa11f665b2a2038796a9; buvid_fp_plain=undefined; SESSDATA=30bfd0ae%2C1730229215%2C78336%2A51CjAI07o7_tWnXOQ6w-zh1BUmkDIGuYzFomATgHOi6zk_RP6DYeSV3Z1381e1sAE-LpYSVkZxblIwWTZpREJyRjRjYXpKZzNXbHFTN285bGtFVVJGSHpLMEVYQnVya2xIdXdhS19BcW9vQjNfSTQ1Nkx4dGJNYjhxOU1iTUxoNnV2RXBlaTcwcUdBIIEC; bili_jct=7bfb3901c5527d97b224d7bbecf76564; buvid_fp=e7cf9c64751eaa11f665b2a2038796a9; CURRENT_QUALITY=80; browser_resolution=1920-919; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUwNjQ2MzQsImlhdCI6MTcxNDgwNTM3NCwicGx0IjotMX0.p8ZCwBF5ER-glXc5nrOLEGp5V5Kqw-crUickaNlGKKY; bili_ticket_expires=1715064574; b_lsid=5C102E2AE_18F4435DE0D; sid=og0wxj4t",
        "User-Agent": "Mozilla/5.0(Windows NT 10.0;Win64;x64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 124.0.0.0 Safari / 537.36",
        #防盗链
        "Referer": "https://www.bilibili.com/video/BV1Np421Q7Pi/?spm_id_from=333.337.search-card.all.click&vd_source=8ab8d4d746498c786c83e948e04c813d"
    }
    response = requests.get(url=url, headers=headers)
    return response

def GetVideo():
    link = 'https://www.bilibili.com/video/BV1Np421Q7Pi/?spm_id_from=333.337.search-card.all.click&vd_source=8ab8d4d746498c786c83e948e04c813d'
    response = GetResponse(link)
    html = response.text
    # 提取视频标题
    title = re.findall('title="(.*?)"',html)[0]
    print(title)
    #提取视频信息
    info = re.findall('window.__playinfo__=(.*?)</script>',html)[0]
    #info -> json 字符串转成json字典
    json_data = json.loads(info)
    #提取视频链接
    video_url = json_data['data']['dash']['video'][0]['baseUrl']
    # 提取音频链接
    audio_url = json_data['data']['dash']['audio'][0]['baseUrl']
    print(video_url)
    print(audio_url)
    return title,video_url,audio_url
def Save(title,video_url,audio_url):
    """保存数据"""
    #获取音频
    video_content = GetResponse(url=video_url).content
    #获取视频
    audio_content = GetResponse(url=audio_url).content
    #保存数据
    with open('video\\'+title+'.mp4',mode='wb') as v:
        v.write(video_content)
    with open('video\\'+title+'.mp3',mode='wb') as a:
        a.write(audio_content)
    #合并成完整的视频
    # cmd = f"ffmpeg -hide_banner -i video\\{title}.mp4 -i video\\{title}.mp3"
    #
    # subprocess.run(cmd)
if __name__ == '__main__':
    # #获取视频信息
    # title,video_url,audio_url = GetVideo()

    #保存数据
    # Save("历史与文艺作品中的四十七浪人", video_url, audio_url)
    cmd = f"ffmpeg -hide_banner -i video\\历史与文艺作品中的四十七浪人.mp4 -i video\\历史与文艺作品中的四十七浪人.mp3 video\\四十七浪人.mp4"

    subprocess.run(cmd)