#导入自动化模块
import time
import re

from DrissionPage import ChromiumPage,ChromiumOptions
#导入csv模块
import  csv
#创建文件对象
f = open('data.csv',mode='w',encoding='utf-8',newline='')
csv_writer = csv.DictWriter(f,fieldnames=[
    '名称',
    '商圈',
    '距离',
    '标签',
    '点评数',
    '评分',
    '金额',
    '牌',
    '客房数'
])
csv_writer.writeheader()
#第一次运行需要加如下代码 path是谷歌浏览区执行文件位置
# path = r'C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chrome.exe'
# ChromiumOptions().set_browser_path(path).save()
#打开浏览器
dp = ChromiumPage()
# 监听数据包
dp.listen.start("json/HotelSearch")
# 访问网站
dp.get("https://hotels.ctrip.com/hotels/list?countryId=1&city=28&checkin=2024/05/03&checkout=2024/05/04&optionId=28&optionType=City&directSearch=0&display=%E6%88%90%E9%83%BD%2C%20%E5%9B%9B%E5%B7%9D%2C%20%E4%B8%AD%E5%9B%BD&crn=1&adult=1&children=0&searchBoxArg=t&travelPurpose=0&ctm_ref=ix_sb_dl&domestic=1&")
#for循环下滑页面
hotelList = [];
for page in range(1,6):
    print(f'正在采集第{page}页的数据内容')
    if page > 4:
        time.sleep(5)
        next_page = dp.ele('css:.btn-box span')
        #判断是否出现点击按钮
        if next_page.text == '搜索更多酒店':
            #点击搜索更多酒店
            next_page.click()
    # 等待数据包加载
    resp = dp.listen.wait()
    # 获取响应数据内容
    json_data = resp.response.body
    # 解析数据 提取酒店信息所在列表
    hotelList += json_data['Response']['hotelList']['list']
    print(hotelList)
    # 提取列表里面的元素
    # for index in hotelList:
    #
    #     dit = {
    #         '名称': index['base']['hotelName'],
    #         '商圈': index['position']['area'],
    #         '距离': index['position']['poi'],
    #         '标签': index['base']['tags'],
    #         '点评数': index['comment']['content'],
    #         '评分': index['score']['number'],
    #         '金额': index['money']['price'],
    #         '牌': index['base']['badge'],
    #     }
    #     csv_writer.writerow(dit)
    #     print(dit)
    #下滑页面到底部
    dp.scroll.to_bottom()
dp.listen.start("json/hotelStaticInfo")
for index in hotelList:
    hotelId = index['base']['hotelId']
    dp.get(
        "https://hotels.ctrip.com/hotels/detail/?hotelId=" + f"{hotelId}" + "&checkIn=2024-05-03&checkOut=2024-05-04&cityId=28&minprice=&mincurr=&adult=1&children=0&ages=&crn=1&curr=&fgt=&stand=&stdcode=&hpaopts=&mproom=&ouid=&shoppingid=&roomkey=&highprice=-1&lowprice=0&showtotalamt=&hotelUniqueKey=")
    res = dp.listen.wait()
    room = "";
    rooms = res.response.body['Response']['hotelInfo']['basic']['label']
    for r in rooms:
        if re.match("^客房数.*", r) is not None:
            room = r;
    dit = {
        '名称': index['base']['hotelName'],
        '商圈': index['position']['area'],
        '距离': index['position']['poi'],
        '标签': index['base']['tags'],
        '点评数': index['comment']['content'],
        '评分': index['score']['number'],
        '金额': index['money']['price'],
        '牌': index['base']['badge'],
        '客房数': room
    }
    csv_writer.writerow(dit)
    print(dit)


